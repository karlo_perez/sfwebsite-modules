
<?php require_once '_header.php'; ?>

<!-- ---------------- Copy as raw HTML to Visual Composer ------------------ -->

    <div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">

        <div class="device">
            <a class="left carousel-control-albert" href="#carousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control-albert" href="#carousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>

            <img class="albert-bg" src="http://surefiresystems.com/wp-content/uploads/2018/02/device-backdrop_albert.png">
        </div>

        <div class="carousel-inner" role="listbox">      

            <div class="item albert active">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/1_quickstart.png" alt="Make Sales" width="460" height="345">
                <div class="carousel-caption">          
                    <div class="right-hand">
                        <h3>QUICK START</h3>
                        <P>Tap to start the payment.</p>
                    </div>
                </div>
            </div>

            <div class="item albert">
                <span class="out-blocker"></span>
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/2_scan-paper-bill.png" alt="Apply Discounts" width="460" height="345">
                <div class="carousel-caption">                   
                    <div class="right-hand">
                        <h3>SCAN PAPER BILL</h3>
                        <p>Scan barcode to manually key in assessment/ reference number.</p>
                    </div> 
                </div>
            </div>   
            
            <div class="item albert">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/3_specify-paper-amount.png" alt="Take Payment" width="460" height="345">
                <div class="carousel-caption">
                    <div class="right-hand">
                        <h3>SPECIFY PAYMENT AMOUNT</h3>
                        <p>Accept full or partial payment.</p>
                    </div>
                </div>
            </div>

            <div class="item albert">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/4_pay-by-credit-card-or-eftpos.png" alt="Issue Receipt" width="460" height="345">
                <div class="carousel-caption">
                    <div class="right-hand">
                        <h3>PAY BY CREDIT CARD OR EFTPOS</h3>
                        <p>Customer selects payment type. Surcharge can be configured by payment method.</p>
                    </div>
                </div>
            </div>

            <div class="item albert">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/5_issue-receipt.png" alt="Issue Receipt" width="460" height="345">
                <div class="carousel-caption">
                    <div class="right-hand">
                        <h3>ISSUE RECEIPT</h3>
                        <p>Customer chooses to whether print the receipt.</p>
                    </div>
                </div>
            </div>
    
        </div>

        <ol class="carousel-indicators">
            <li data-target="#carousel" data-slide-to="0" class="active"></li>
            <li data-target="#carousel" data-slide-to="1"></li>
            <li data-target="#carousel" data-slide-to="2"></li>
            <li data-target="#carousel" data-slide-to="3"></li>
            <li data-target="#carousel" data-slide-to="4"></li>
        </ol>
    </div> <!-- END #carousel [ > 540px ] -->

    
    <div id="modals" class="">

        <a class="modal-trigger" data-toggle="modal" data-target="#modal-1"  data-dynamic="true">QUICK START</a>

        <a class="modal-trigger" data-toggle="modal" data-target="#modal-2"  data-dynamic="true">SCAN PAPER BILL</a>

        <a class="modal-trigger" data-toggle="modal" data-target="#modal-3"  data-dynamic="true">SPECIFY PAYMENT AMOUNT</a>
        
        <a class="modal-trigger" data-toggle="modal" data-target="#modal-4"  data-dynamic="true">PAY BY CREDIT CAR OR EFTPOS</a>

        <a class="modal-trigger" data-toggle="modal" data-target="#modal-5"  data-dynamic="true">ISSUE RECEIPT</a>


        <div class="modal" id="modal-1">
            <a class="modal-button-close" data-dismiss="modal" aria-label="close">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            <div class="modal-caption">
                <h3>QUICK START</h3>
                <P>Tap to start the payment.</p>
            </div>
            <div class="modal-image">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/1_quickstart.png" alt="Make Sales" width="460" height="345">
            </div>
        </div> <!- -END #modal-1 -->

        <div class="modal" id="modal-2">
            <a class="modal-button-close" data-dismiss="modal" aria-label="close">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            <div class="modal-caption">
                <h3>SCAN PAPER BILL</h3>
                <p>Scan barcode to manually key in assessment/reference number.</p>
            </div>
            <div class="modal-image">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/2_scan-paper-bill.png" alt="Make Sales" width="460" height="345">
            </div>
        </div> <!- -END #modal-2 -->

        <div class="modal" id="modal-3">
            <a class="modal-button-close" data-dismiss="modal" aria-label="close">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            <div class="modal-caption">
                <h3>SPECIFY PAYMENT AMOUNT</h3>
                <p>Accept full or partial payment.</p>
            </div>
            <div class="modal-image">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/3_specify-paper-amount.png" alt="Make Sales" width="460" height="345">
            </div>
        </div> <!- -END #modal-3 -->

        <div class="modal" id="modal-4">
            <a class="modal-button-close" data-dismiss="modal" aria-label="close">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            <div class="modal-caption">
                <h3>PAY BY CREDIT CARD OR EFTPOS</h3>
                <p>Customer selects payment type. Surcharge can be configured by payment method.</p>
            </div>
            <div class="modal-image">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/4_pay-by-credit-card-or-eftpos.png" alt="Make Sales" width="460" height="345">
            </div>
        </div> <!- -END #modal-4 -->

        <div class="modal" id="modal-5">
            <a class="modal-button-close" data-dismiss="modal" aria-label="close">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            <div class="modal-caption">
                <h3>ISSUE RECEIPT</h3>
                <p>Customer chooses to whether print the receipt.</p>
            </div>
            <div class="modal-image">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/5_issue-receipt.png" alt="Make Sales" width="460" height="345">
            </div>
        </div> <!- -END #modal-5 -->

    </div> <!-- END #modals [ <= 540px ] -->
    

<!-- ----------------------- END of copy as raw HTML --------------------------- -->

<?php require_once '_functions.php'; ?>

</body>
</html>