
<?php require_once '_header.php'; ?>

<!-- ---------------- Copy as raw HTML to Visual Composer ------------------ -->

    <div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">

        <div class="carousel-inner" role="listbox">

            <div class="device">
                <a class="left carousel-control-blade" href="#carousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control-blade" href="#carousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

                <img class="blade-bg" src="http://surefiresystems.com/wp-content/uploads/2018/02/device-backdrop_anz-blade.png">
            </div>

            <div class="item active">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/1_make-sales.png" alt="Make Sales" width="460" height="345">
                <div class="carousel-caption">          
                    <div class="right-hand">
                        <h3>MAKE SALES</h3>
                        <P>Capture product or service sales by:</p>
                        <ul>
                            <li>scanning barcodes</li>
                            <li>browsing by category or favourites</li>
                            <li>searching by keyword.</li>
                        <ul>
                    </div>
                </div>
            </div>

            <div class="item">
                <span class="out-blocker"></span>
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/2_apply-discounts.png" alt="Apply Discounts" width="460" height="345">
                <div class="carousel-caption">                   
                    <div class="right-hand">
                        <h3>APPLY DISCOUNTS</h3>
                        <p>Apply discounts by amount or percentage or apply a new price for an item.</p>
                    </div> 
                </div>
            </div>   
            
            <div class="item">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/3_take-payment.png" alt="Take Payment" width="460" height="345">
                <div class="carousel-caption">
                    <div class="right-hand">
                        <h3>TAKE PAYMENT</h3>
                        <p>Accept payment by credit/debit card, cash or on account.</p>
                    </div>
                </div>
            </div>

            <div class="item">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/4_issue-receipt.png" alt="Issue Receipt" width="460" height="345">
                <div class="carousel-caption">
                    <div class="right-hand">
                        <h3>ISSUE RECEIPT</h3>
                        <p>Print a receipt on your Bluetooth printer or email directly to your customer.</p>
                    </div>
                </div>
            </div>
    
        </div>

        <ol class="carousel-indicators">
            <li data-target="#carousel" data-slide-to="0" class="active"></li>
            <li data-target="#carousel" data-slide-to="1"></li>
            <li data-target="#carousel" data-slide-to="2"></li>
            <li data-target="#carousel" data-slide-to="3"></li>
        </ol>
    </div> <!-- END #carousel [ > 540px ] -->

    
    <div id="modals" class="">

        <a class="modal-trigger" data-toggle="modal" data-target="#modal-1"  data-dynamic="true">MAKE SALES</a>

        <a class="modal-trigger" data-toggle="modal" data-target="#modal-2"  data-dynamic="true">APPLY DISCOUNTS</a>

        <a class="modal-trigger" data-toggle="modal" data-target="#modal-3"  data-dynamic="true">TAKE PAYMENT</a>
        
        <a class="modal-trigger" data-toggle="modal" data-target="#modal-4"  data-dynamic="true">ISSSUE RECEIPT</a>


        <div class="modal" id="modal-1">
            <a class="modal-button-close" data-dismiss="modal" aria-label="close">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            <div class="modal-caption">
                <h3>MAKE SALES</h3>
                <P>Capture product or service sales by:</p>
                <ul>
                    <li>scanning barcodes</li>
                    <li>browsing by category or favourites</li>
                    <li>searching by keyword.</li>
                <ul>
            </div>
            <div class="modal-image">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/1_make-sales.png" alt="Make Sales" width="460" height="345">
            </div>
        </div> <!- -END #modal-1 -->

        <div class="modal" id="modal-2">
            <a class="modal-button-close" data-dismiss="modal" aria-label="close">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            <div class="modal-caption">
                <h3>APPLY DISCOUNTS</h3>
                <p>Apply discounts by amount or percentage or apply a new price for an item.</p>
            </div>
            <div class="modal-image">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/2_apply-discounts.png" alt="Make Sales" width="460" height="345">
            </div>
        </div> <!- -END #modal-2 -->

        <div class="modal" id="modal-3">
            <a class="modal-button-close" data-dismiss="modal" aria-label="close">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            <div class="modal-caption">
                <h3>TAKE PAYMENT</h3>
                <p>Accept payment by credit/debit card, cash or on account.</p>
            </div>
            <div class="modal-image">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/3_take-payment.png" alt="Make Sales" width="460" height="345">
            </div>
        </div> <!- -END #modal-3 -->

        <div class="modal" id="modal-4">
            <a class="modal-button-close" data-dismiss="modal" aria-label="close">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            <div class="modal-caption">
                <h3>ISSUE RECEIPT</h3>
                <p>Print a receipt on your Bluetooth printer or email directly to your customer.</p>
            </div>
            <div class="modal-image">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/4_issue-receipt.png" alt="Make Sales" width="460" height="345">
            </div>
        </div> <!- -END #modal-4 -->

    </div> <!-- END #modals [ <= 540px ] -->
    

<!-- ----------------------- END of copy as raw HTML --------------------------- -->

<?php require_once '_functions.php'; ?>
</body>
</html>