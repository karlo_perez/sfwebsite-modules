
<?php require_once '_header.php'; ?>

<!-- ---------------- Copy as raw HTML to Visual Composer ------------------ -->

    <div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">

        <div class="device">
            <a class="left carousel-control-albert" href="#carousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control-albert" href="#carousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>

            <img class="albert-bg" src="http://surefiresystems.com/wp-content/uploads/2018/02/device-backdrop_albert.png">
        </div>

        <div class="carousel-inner" role="listbox">

            <div class="item albert active">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/04_home-images-candyShop-high.png" alt="Sell Products" width="460" height="345">
                <div class="carousel-caption">          
                    <div class="right-hand">
                        <h3>SELL PRODUCTS</h3>
                        <P>Sell food and drinks by scanning bar codes or selecting from the Food and Beverage and Concession Sales menus.</p>
                    </div>
                </div>
            </div>

            <div class="item albert">
                <span class="out-blocker"></span>
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/015_charge-new-giftCard-high.png" alt="Sell Gift Cards" width="460" height="345">
                <div class="carousel-caption">                   
                    <div class="right-hand">
                        <h3>SELL GIFT CARDS</h3>
                        <p>Sell gift cards by scanning and activating on the spot.</p>
                    </div> 
                </div>
            </div>   
            
            <div class="item albert">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/037_payment-6-high.png" alt="Take Payment" width="460" height="345">
                <div class="carousel-caption">
                    <div class="right-hand">
                        <h3>TAKE PAYMENT</h3>
                        <p>Take credit or debit card, gift card or cash payments.</p>
                    </div>
                </div>
            </div>

            <div class="item albert">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/039_transaction-complete-receipt-8-high.png" alt="Issue Receipt" width="460" height="345">
                <div class="carousel-caption">
                    <div class="right-hand">
                        <h3>ISSUE RECEIPT</h3>
                        <p>Print a receipt on your Bluetooth printer or email directly to your customer.</p>
                    </div>
                </div>
            </div>

            <div class="item albert">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/011_product-details-high.png" alt="Select Movie and Seat" width="460" height="345">
                <div class="carousel-caption">
                    <div class="right-hand">
                        <h3>SELECT MOVIE AND SEAT</h3>
                        <p>Customer selects their movie and seat. The user experience is designed specifically for customer self-service.</p>
                    </div>
                </div>
            </div>

            <div class="item albert">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/011_product-Rolls-details-high.png" alt="Place Order" width="460" height="345">
                <div class="carousel-caption">
                    <div class="right-hand">
                        <h3>PLACE ORDER</h3>
                        <p>Customer places food and drinks order and nominates when they would like it delivered.</p>
                    </div>
                </div>
            </div>
    
        </div>

        <ol class="carousel-indicators">
            <li data-target="#carousel" data-slide-to="0" class="active"></li>
            <li data-target="#carousel" data-slide-to="1"></li>
            <li data-target="#carousel" data-slide-to="2"></li>
            <li data-target="#carousel" data-slide-to="3"></li>
            <li data-target="#carousel" data-slide-to="4"></li>
            <li data-target="#carousel" data-slide-to="5"></li>
        </ol>
    </div> <!-- END #carousel [ > 540px ] -->

    
    <div id="modals" class="">

        <a class="modal-trigger" data-toggle="modal" data-target="#modal-1"  data-dynamic="true">SELL PRODUCTS</a>

        <a class="modal-trigger" data-toggle="modal" data-target="#modal-2"  data-dynamic="true">SELL GIFT CARDS</a>

        <a class="modal-trigger" data-toggle="modal" data-target="#modal-3"  data-dynamic="true">TAKE PAYMENT</a>
        
        <a class="modal-trigger" data-toggle="modal" data-target="#modal-4"  data-dynamic="true">ISSSUE RECEIPT</a>

        <a class="modal-trigger" data-toggle="modal" data-target="#modal-5"  data-dynamic="true">SELECT MOVIE AND SEAT</a>

        <a class="modal-trigger" data-toggle="modal" data-target="#modal-6"  data-dynamic="true">PLACE ORDER</a>


        <div class="modal" id="modal-1">
            <a class="modal-button-close" data-dismiss="modal" aria-label="close">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            <div class="modal-caption">
                <h3>SELL PRODUCTS</h3>
                <P>Sell food and drinks by scanning bar codes or selecting from the Food and Beverage and Concession Sales menus.</p>
            </div>
            <div class="modal-image">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/04_home-images-candyShop-high.png" alt="Make Sales" width="460" height="345">
            </div>
        </div> <!- -END #modal-1 -->

        <div class="modal" id="modal-2">
            <a class="modal-button-close" data-dismiss="modal" aria-label="close">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            <div class="modal-caption">
                <h3>SELL GIFT CARDS</h3>
                <p>Sell gift cards by scanning and activating on the spot.</p>
            </div>
            <div class="modal-image">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/015_charge-new-giftCard-high.png" alt="Make Sales" width="460" height="345">
            </div>
        </div> <!- -END #modal-2 -->

        <div class="modal" id="modal-3">
            <a class="modal-button-close" data-dismiss="modal" aria-label="close">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            <div class="modal-caption">
                <h3>TAKE PAYMENT</h3>
                <p>Take credit or debit card, gift card or cash payments.</p>
            </div>
            <div class="modal-image">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/037_payment-6-high.png" alt="Make Sales" width="460" height="345">
            </div>
        </div> <!- -END #modal-3 -->

        <div class="modal" id="modal-4">
            <a class="modal-button-close" data-dismiss="modal" aria-label="close">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            <div class="modal-caption">
                <h3>ISSUE RECEIPT</h3>
                <p>Print a receipt on your Bluetooth printer or email directly to your customer.</p>
            </div>
            <div class="modal-image">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/039_transaction-complete-receipt-8-high.png" alt="Make Sales" width="460" height="345">
            </div>
        </div> <!- -END #modal-4 -->

        <div class="modal" id="modal-5">
            <a class="modal-button-close" data-dismiss="modal" aria-label="close">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            <div class="modal-caption">
                <h3>SELECT MOVIE AND SEAT</h3>
                <p>Customer selects their movie and seat. The user experience is designed specifically for customer self-service.</p>
            </div>
            <div class="modal-image">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/011_product-details-high.png" alt="Make Sales" width="460" height="345">
            </div>
        </div> <!- -END #modal-5 -->

        <div class="modal" id="modal-6">
            <a class="modal-button-close" data-dismiss="modal" aria-label="close">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            <div class="modal-caption">
                <h3>PLACE ORDER</h3>
                <p>Customer places food and drinks order and nominates when they would like it delivered.</p>
            </div>
            <div class="modal-image">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/011_product-Rolls-details-high.png" alt="Make Sales" width="460" height="345">
            </div>
        </div> <!- -END #modal-6 -->

    </div> <!-- END #modals [ <= 540px ] -->
    

<!-- ----------------------- END of copy as raw HTML --------------------------- -->

<?php require_once '_functions.php'; ?>
</body>
</html>