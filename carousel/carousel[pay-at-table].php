
<?php require_once '_header.php'; ?>

<!-- ---------------- Copy as raw HTML to Visual Composer ------------------ -->

    <div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">

        <div class="device">
            <a class="left carousel-control-blade" href="#carousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control-blade" href="#carousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>

            <img class="blade-bg" src="http://surefiresystems.com/wp-content/uploads/2018/02/device-backdrop_anz-blade.png">
        </div>

        <div class="carousel-inner" role="listbox">

            <div class="item active">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/1_select-table.png" alt="Select Table" width="460" height="345">
                <div class="carousel-caption">          
                    <div class="right-hand">
                        <h3>SELECT TABLE</h3>
                        <P>Retrieve orders placed on POS terminal</p>
                    </div>
                </div>
            </div>

            <div class="item">
                <span class="out-blocker"></span>
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/2_review-order-details.png" alt="Review Order Details" width="460" height="345">
                <div class="carousel-caption">                   
                    <div class="right-hand">
                        <h3>REVIEW ORDER DETAILS</h3>
                        <p>Review details of orders including modifiers and table number</p>
                    </div> 
                </div>
            </div>   
            
            <div class="item">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/3_split-bill.png" alt="Split Bill" width="460" height="345">
                <div class="carousel-caption">
                    <div class="right-hand">
                        <h3>SPLIT BILL</h3>
                        <p>Split the bill equally between diners or by item</p>
                    </div>
                </div>
            </div>

            <div class="item">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/4_take-payment.png" alt="Take Payment Including Tips" width="460" height="345">
                <div class="carousel-caption">
                    <div class="right-hand">
                        <h3>TAKE PAYMENT INCLUDING TIPS</h3>
                        <p>Accept payment by card or cash and tips by percentage or dollar amount</p>
                    </div>
                </div>
            </div>

            <div class="item">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/5_add-up.png" alt="Add Tip" width="460" height="345">
                <div class="carousel-caption">
                    <div class="right-hand">
                        <h3>ADD TIP</h3>
                        <p>...</p>
                    </div>
                </div>
            </div>

            <div class="item">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/6_issue-receipt.png" alt="Issue Receipts" width="460" height="345">
                <div class="carousel-caption">
                    <div class="right-hand">
                        <h3>ISSUE RECEIPTS</h3>
                        <p>Print a receipt or email directly to the customer</p>
                    </div>
                </div>
            </div>
    
        </div>

        <ol class="carousel-indicators">
            <li data-target="#carousel" data-slide-to="0" class="active"></li>
            <li data-target="#carousel" data-slide-to="1"></li>
            <li data-target="#carousel" data-slide-to="2"></li>
            <li data-target="#carousel" data-slide-to="3"></li>
            <li data-target="#carousel" data-slide-to="4"></li>
            <li data-target="#carousel" data-slide-to="5"></li>
        </ol>
    </div> <!-- END #carousel [ > 540px ] -->

    
    <div id="modals" class="">

        <a class="modal-trigger" data-toggle="modal" data-target="#modal-1"  data-dynamic="true">SELECT TABLE</a>

        <a class="modal-trigger" data-toggle="modal" data-target="#modal-2"  data-dynamic="true">REVIEW ORDER DETAILS</a>

        <a class="modal-trigger" data-toggle="modal" data-target="#modal-3"  data-dynamic="true">SPLIT BILL</a>
        
        <a class="modal-trigger" data-toggle="modal" data-target="#modal-4"  data-dynamic="true">TAKE PAYMENT INCLUDING TIPS</a>

        <a class="modal-trigger" data-toggle="modal" data-target="#modal-5"  data-dynamic="true">ADD TIP</a>

        <a class="modal-trigger" data-toggle="modal" data-target="#modal-6"  data-dynamic="true">ISSUE RECEIPTS</a>


        <div class="modal" id="modal-1">
            <a class="modal-button-close" data-dismiss="modal" aria-label="close">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            <div class="modal-caption">
                <h3>SELECT TABLE</h3>
                <P>Retrieve orders placed on POS terminal</p>
            </div>
            <div class="modal-image">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/1_select-table.png" alt="Make Sales" width="460" height="345">
            </div>
        </div> <!- -END #modal-1 -->

        <div class="modal" id="modal-2">
            <a class="modal-button-close" data-dismiss="modal" aria-label="close">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            <div class="modal-caption">
                <h3>REVIEW ORDER DETAILS</h3>
                <p>Review details of orders including modifiers and table number</p>
            </div>
            <div class="modal-image">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/2_review-order-details.png" alt="Make Sales" width="460" height="345">
            </div>
        </div> <!- -END #modal-2 -->

        <div class="modal" id="modal-3">
            <a class="modal-button-close" data-dismiss="modal" aria-label="close">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            <div class="modal-caption">
                <h3>SPLIT BILL</h3>
                <p>Split the bill equally between diners or by item</p>
            </div>
            <div class="modal-image">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/3_split-bill.png" alt="Make Sales" width="460" height="345">
            </div>
        </div> <!- -END #modal-3 -->

        <div class="modal" id="modal-4">
            <a class="modal-button-close" data-dismiss="modal" aria-label="close">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            <div class="modal-caption">
                <h3>TAKE PAYMENT INCLUDING TIPS</h3>
                <p>Accept payment by card or cash and add tips by percentage or dollar amount</p>
            </div>
            <div class="modal-image">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/4_take-payment.png" alt="Make Sales" width="460" height="345">
            </div>
        </div> <!- -END #modal-4 -->

        <div class="modal" id="modal-5">
            <a class="modal-button-close" data-dismiss="modal" aria-label="close">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            <div class="modal-caption">
                <h3>ADD TIP</h3>
                <p>...</p>
            </div>
            <div class="modal-image">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/5_add-up.png" alt="Make Sales" width="460" height="345">
            </div>
        </div> <!- -END #modal-5 -->

        <div class="modal" id="modal-6">
            <a class="modal-button-close" data-dismiss="modal" aria-label="close">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            <div class="modal-caption">
                <h3>ISSUE RECEIPTS</h3>
                <p>Print a receipt or email directly to the customer</p>
            </div>
            <div class="modal-image">
                <img src="http://surefiresystems.com/wp-content/uploads/2018/02/6_issue-receipt.png" alt="Make Sales" width="460" height="345">
            </div>
        </div> <!- -END #modal-6 -->

    </div> <!-- END #modals [ <= 540px ] -->
    

<!-- ----------------------- END of copy as raw HTML --------------------------- -->

<?php require_once '_functions.php'; ?>
</body>
</html>